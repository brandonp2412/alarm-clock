# Alarm Clock

Alarm Clock waits for a given amount of time, and then alerts the user
when that time is up.

# Installation

```
git clone https://gitlab.com/brandonp2412/alarm-clock ~/alarm-clock
echo 'export PATH=$PATH:~/alarm-clock/bin' >> ~/.profile
```

# Usage

```
alarm ab "$TITLE"
```

Where `a` is any number followed by an **h**, e.g. `1h`,
and `b` is any number followed by an **m**, e.g. 1m.

Hour parameter:
```
alarm 1h "Washing"
Washing - 00:00
```

Minute parameter:
```
alarm 30m "Dinner"
Dinner - 00:00
```

Both:
```
alarm 1h30m "Dryer"
Dryer - 00:00
```

# Dependencies

- [Bash](https://www.gnu.org/savannah-checkouts/gnu/bash/manual/bash.html)
